# I wrote a Code and a Game class. My Code class represented a
# sequence of four pegs. The Game kept track of how many turns
#  has passed, the correct Code, prompt the user for input.
#
# I wrote a Code::random class method which builds a Code
# instance with random peg colors. I also wrote a Code::parse(input)
#  method that took a user input string like "RGBY" and built a
#  Code object. I made code objects for both (1) the secret code
#  and (2) the user's guess of the code. I wrote methods like
#  Code#exact_matches(other_code) and Code#near_matches(other_code)
#to handle comparisons.
require 'byebug'

class Code
  attr_reader :pegs

  PEGS = { R: "R", G: "G", B: "B", Y: "Y", O: "O", P: "P" }

  def initialize(arr_pegs)
    @pegs = arr_pegs

  end

  def self.parse(color_str)
    color_str = color_str.upcase.chars
    if color_str.all? { |color| Code.valid_color? color }
      #code = code.pegs
      Code.new(color_str)
    else
      raise "Invalid color entered"
    end

  end

  def self.valid_color?(color)
    "RGBYOP".include? color
  end

  def self.random
    all_colors = %i{R G B Y O P}
    rand_index = rand(6)
    rand_colors = Array.new
    4.times do
      color = all_colors.shuffle[rand_index]
      rand_colors <<  PEGS[color]
    end
    Code.new(rand_colors)
  end

  def [](index)
    @pegs[index]
  end

  def ==(code)
    return false if code.class != Code
    @pegs == code.pegs
  end

  def exact_matches(code)
    color_str = code.pegs.join.upcase
    color_arr = color_str.chars
    counter = 0
    @pegs.each_with_index do |peg_guess, indx|
      counter += 1 if peg_guess.upcase == color_arr[indx]
    end
    counter
  end

  def near_matches(code)
    counter_hash1 = Hash.new(0)
    counter_hash2 = Hash.new(0)

    @pegs.each { |color| counter_hash1[color] += 1 }
    code.pegs.each { |color| counter_hash2[color] += 1 }

    total_matches = @pegs.uniq.map do |color|
      [[counter_hash1[color]], [counter_hash2[color]]].min
    end
    total_matches = total_matches.flatten.reduce(:+)
    total_matches - self.exact_matches(code) # = near_matches

  end

end

class Game
  attr_reader :secret_code


  def initialize(code = nil)
    if code.nil?
      @secret_code = Code.random
    else @secret_code = code
    end
  end

  def get_guess
    Code.parse @secret_code.pegs.join
  end

  def display_matches(code)

    near = @secret_code.near_matches code
    exact = @secret_code.exact_matches code
    print "near matches: #{near} and exact matches: #{exact}"
  end

end
